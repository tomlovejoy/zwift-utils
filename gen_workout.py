#!/usr/bin/evn python3

fout = open("xp_monster_test.zwo", "w")
fout.write("""
<workout_file>
    <author>Kenny k</author>
    <name>XP Monster Testing</name>
    <description>Earn XP as fast as possible, no matter the route</description>
    <sportType>bike</sportType>
    <tags>
    </tags>
    <workout>
           <SteadyState Duration="62" Power="0.6" MinPower="0.2" MaxPower="1.2" />
           <SteadyState Duration="120" Power="0.6" NerverFails="1" />
""")

total = 0

for interval in range(1,31):
    total += interval
    fout.write(f'        <SteadyState Duration="{interval}" Power="0.6"/>\n')

for interval in range(1,11):
    for rest in range(1,6):
        fout.write(f'        <IntervalsT Repeat="1" OnDuration="{interval}" OffDuration="{rest}" OnPower="0.6" OffPower="0.6"/>\n')
        total += interval + rest

for interval in range(11,61):
    rest = 5
    fout.write(f'        <IntervalsT Repeat="1" OnDuration="{interval}" OffDuration="{rest}" OnPower="0.6" OffPower="0.6"/>\n')
    total += interval + rest

fout.write("""
    </workout>
</workout_file>
""")
print(total/60)

